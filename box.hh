class Box
{
    public:
        Box();
        double length; // length of a box
        double width;  // width of a box
        double height; // height of a box

        void report();
        void setWidth(double inWidth);
};
