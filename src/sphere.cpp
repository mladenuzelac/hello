#include <iostream>
#include "sphere.hh"

using namespace std;

Sphere::Sphere() {
    diameter = 5;
}

void Sphere::report() {
    cout << "     Yo, what's up:  " << endl;
    cout << "     diameter:  " << diameter <<  endl;
}

void Sphere::setDiameter(double inD) {
    diameter = inD;
}
