#include <iostream>
#include "box.hh"

using namespace std;

int main(int argc, char** argv)
{
     
    Box box1;
    Box box2;
    Box boxA;
    Box boxB;
    Box boxD;
    Box boxE;

    box1.setWidth(15.0);
    cout<<"Hello world!"<<endl;
    cout<<"Box1"<<endl;
    box1.report();
    cout<<"Box2:"<<endl;
    box2.report();
    cout<<"BoxA:"<<endl;
    boxA.report();
    cout<<"BoxE:"<<endl;
    boxE.report();

    // always return zero
    // comment here
    return 0;
}
