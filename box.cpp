#include <iostream>
#include "box.hh"

using namespace std;

Box::Box() {
    length = 1;
    width  = 4;
    height = 5;
}

void Box::report(){
    cout << "length: " << length << endl;
    cout << "width:  " << width << endl;
    cout << "height: " << height << endl;
}

void Box::setWidth(double inWidth){
    width = inWidth;
}

